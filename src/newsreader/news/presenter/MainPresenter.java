package newsreader.news.presenter;

import newsreader.news.model.MainModel;
import newsreader.news.view.MainView;

public class MainPresenter {
	public static void main(String []args){

		MainView vista=MainView.getInstance();
		MainModel modelo=MainModel.getInstance();
		new MainScreenPresenter(vista.getMainScreen(),modelo.getNewsModel());
	}

}
