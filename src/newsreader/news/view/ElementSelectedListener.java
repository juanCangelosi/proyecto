package newsreader.news.view;

/**
 * Interface ElementSelectedListener
 * 
 * Esta es la interfaz del listener de evento de selecci�n de item en la lista de noticias.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */

public interface ElementSelectedListener {

	/**
	 * 
	 * Este m�todo es llamado por mainscreen y es disparado por el evento de usuario t�tulo seleccionado.
	 * @param index
	 *          argumento de tipo int que indica el indice de la noticia correspondiente en el total de noticias.	 
	 * 
	 */	
	public void onElementSelected(int index);
}
