package newsreader.news.view;

/**
 * Class MainScreenImpl
 * 
 * Esta clase implementa la interfaz MainScreen. Se encarga de inicializar la pantalla principal y mostrar las noticias.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebView;

class MainScreenImpl implements MainScreen {

	private List<ElementSelectedListener> listeners;
	private JFrame frame;	
	private JFXPanel fxPanel ;
	private JList<String> list;

	/**
	 * 
	 * Se encarga de inicializar la lista de listeners de eventos de seleccion de elementos, y delega el resto de la inicializaci�n
	 * al metodo initialize().
	 * 
	 */
	protected MainScreenImpl(){
		listeners= new ArrayList<>();
		initialize();
	}

	/**
	 * 
	 * Este m�todo se encarga de inicializar un JFrame que ser� el frame principal del sistema. Agrega un JFXPanel para visualizar las p�ginas.
	 * 
	 * 
	 */	
	private void initialize() {

		try {
			UIManager.setLookAndFeel("com.easynth.lookandfeel.EaSynthLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		frame = new JFrame();
		frame.setPreferredSize(new Dimension( 1400, 700));

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		list = new JList<String>();
		list.setMaximumSize(new Dimension(15, 0));
		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setPreferredSize(new Dimension(550,0));
		
		frame.getContentPane().add(scrollPane, BorderLayout.WEST);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.EAST);

		fxPanel = new JFXPanel();
		panel.add(fxPanel);

		frame.setTitle("NewsReader");
		frame.pack();
		frame.setVisible(true);


	}

	public void setNews(String[] news) {
		list.setModel(new AbstractListModel<String>() {

			private static final long serialVersionUID = 1L;
			public int getSize() {
				return news.length;
			}
			public String getElementAt(int index) {
				return news[index];
			}
		});
		list.addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent e) {

				if (!e.getValueIsAdjusting()) {

					Platform.runLater(() -> {
						for(ElementSelectedListener element:listeners){
							element.onElementSelected(list.getSelectedIndex());
						}
					});
				}				;
			}
		});

		list.setSelectedIndex(0);
	}

	public void addElementSelectedListener(ElementSelectedListener l) {
		listeners.add(l);

	}


	public void loadWebView(String url) {
		Platform.runLater(() -> {WebView webView = new WebView();
		fxPanel.setScene(new Scene(webView));
		webView.getEngine().load(url);
		});
	}

}
