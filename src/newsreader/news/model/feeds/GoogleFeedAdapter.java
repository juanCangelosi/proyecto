package newsreader.news.model.feeds;

import apimodel.GoogleAPIFeed;

public class GoogleFeedAdapter extends Feed {
	
	public GoogleFeedAdapter(GoogleAPIFeed f){
		super(f.getTitle(),f.getLink());	
	}

}
