package newsreader.news.model.newsapiuse;

import java.util.LinkedList;
import java.util.List;

import com.comision4.newfetcher.NewsAPI;
import com.comision4.newfetcher.logic.AtomNewsFetcher;
import com.comision4.newfetcher.model.AtomFeed;
import com.comision4.newfetcher.model.NoURLSetException;

import newsreader.news.model.feeds.Feed;
import newsreader.news.model.feeds.AtomFeedAdapter;

class AtomAPI implements NewsApiUse{
	private List<String> urls;
	
	public AtomAPI(){
		urls= new LinkedList<String>();
	}
	
	public List<Feed> useAPI() {
		List<Feed> feeds = new LinkedList<Feed>();
		NewsAPI api = NewsAPI.getInstance();
		AtomNewsFetcher fetcher = api.getAtomNewsFetcher();
		for(String url: urls){
			fetcher.addUrl(url);
		}
		
		List<AtomFeed> feedsAAdaptar = new LinkedList<AtomFeed>();
		try {
			feedsAAdaptar = fetcher.getNews();
			for(AtomFeed af : feedsAAdaptar){
				feeds.add(new AtomFeedAdapter(af));
			}
		} catch (NoURLSetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return feeds;
	}
	
	public void addUrl(String url){
		urls.add(url);
	}
	
	

}
