package newsreader.news.model.newsapiuse;

import java.util.LinkedList;
import java.util.List;

import newsreader.news.model.feeds.Feed;
import newsreader.news.model.feeds.RSSFeedAdapter;
import rssfeed.api.logic.MainRSS;
import rssfeed.api.model.EmptyUrlException;
import rssfeed.api.model.Feed_RSS;

class RssAPI implements NewsApiUse {
	private List<String> urls;
	
	public RssAPI(){
		urls= new LinkedList<String>();
	}
	
	public List<Feed> useAPI(){
		List<Feed> feeds = new LinkedList<Feed>();
		MainRSS mainRSS = MainRSS.getInstance();	 //solicito al conector la instancia de la api
		for(String url: urls){
			mainRSS.getModuloURLs().addURL(url); //a�ado una url a la api 
		}
		
		Feed_RSS[] feedsApi = null;
		try {
			 feedsApi = mainRSS.getModuloNews().getNews();
			 for(int i=0; i<feedsApi.length; i++){
				 feeds.add(new RSSFeedAdapter(feedsApi[i]));
			 }
			// consulta a la api sobre las noticias y esta se las devuelve correctamente un arreglo Feed_RSS.
		}
		catch(EmptyUrlException e) {
					//c�digo del manejo de la excepci�n en caso de no haber urls en el URLContainer
			e.printStackTrace();
		}
		return feeds;
	}
	
	public void addUrl(String url){
		urls.add(url);
	}
}
