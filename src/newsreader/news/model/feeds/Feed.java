package newsreader.news.model.feeds;

/**
 * Class Feed
 * 
 * Esta clase representa  una abstracci�n del objeto de dato noticia. Almacena el t�tulo y el link de la noticia.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */

public class Feed {

	protected String title;
	protected String link;

	/**
	 * 
	 * Se encarga de inicializar las propiedades privadas title y link, ambas de tipo String.
	 * 
	 * @param t
	 *            objeto de tipo String que ser� el t�tulo de la noticia.
	 * @param l
	 *            objeto de tipo String que ser� el link de la noticia.
	 * 
	 */
	public Feed(String t, String l){
		title=t;
		link=l;
	}

	/**
	 * Asigna un nuevo t�tulo a la noticia.
	 * @param t Nuevo titulo a asignar.
	 */	
	public void setTitle(String t){
		title=t;
	}

	/**
	 * Asigna un nuevo link a la noticia.
	 * @param l Nuevo link a asignar.
	 */	
	public void setLink(String l){
		link=l;

	}

	/**
	 * Devuelve el titulo de la noticia en un String.
	 * @return title Titulo de la noticia.
	 */
	public String getTitle(){
		return title;
	}

	/**
	 * Devuelve el link de la noticia en un String.
	 * @return link Link de la noticia.
	 */
	public String getLink(){
		return link;
	}

}
