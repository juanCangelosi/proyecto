package newsreader.news.model.newsapiuse;

import java.util.LinkedList;
import java.util.List;

public class Creator {

	private List<NewsApiUse> apis;
	
	public Creator(){
		
		apis= new LinkedList<NewsApiUse>();
		NewsApiUse atomApi = new AtomAPI();
		atomApi.addUrl("http://www.theregister.co.uk/science/headlines.atom");
		atomApi.addUrl("https://www.theregister.co.uk/emergent_tech/internet_of_things/headlines.atom");
		
		NewsApiUse rssApi = new RssAPI();
		rssApi.addUrl("https://www.sciencedaily.com/rss/top/science.xml");
		rssApi.addUrl("http://feeds.bbci.co.uk/news/science_and_environment/rss.xml?edition=uk");
		
		NewsApiUse googleApi = new GoogleAPI();
		googleApi.addUrl("https://newsapi.org/v1/articles?source=hacker-news&sortBy=top&apiKey=");
		googleApi.addUrl("https://newsapi.org/v1/articles?source=google-news&sortBy=top&apiKey=");
		
		apis.add(atomApi);
		apis.add(googleApi);
		apis.add(rssApi);
	
	}
	
	public List<NewsApiUse> getApis(){
		return apis;
	}
	
}
