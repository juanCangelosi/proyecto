package newsreader.news.model;

import java.util.LinkedList;
import java.util.List;

import newsreader.news.model.feeds.Feed;
import newsreader.news.model.newsapiuse.NewsApiUse;

class NewsModelImp implements NewsModel{

	private List<NewsApiUse> apis;
	
	protected NewsModelImp(){


	}

	public List<Feed> getNews() {
		List<Feed>  feedsARetornar;
		
		List<List<Feed>> feedLists= new LinkedList<List<Feed>>();
		for(NewsApiUse n : apis){
			List<Feed> listaApi = n.useAPI();
			feedLists.add(listaApi);
		}
		
		feedsARetornar= new LinkedList<Feed>();
		for(List<Feed> feedList: feedLists){
			for(Feed f : feedList){
				feedsARetornar.add(f);
			}
		}
		
		return feedsARetornar;
	}
	
	public void setAPIList(List<NewsApiUse> l){
		apis=l;
	}
	
}
