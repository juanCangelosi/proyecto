package newsreader.news.view;

/**
 * Interface MainScreen 
 * 
 * Esta es la interfaz p�blica de la pantalla principal. Expone los m�todos de interaccion con la misma.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */

public interface MainScreen {

	/**
	 * 
	 * Este m�todo se encarga de actualizar la lista de t�tulos de noticias.
	 * @param news Arreglo de tipo String que contiene los titulos de las noticias.		 
	 * 
	 */	
	public void setNews(String[] news);

	/**
	 * 
	 * Este m�todo se encarga de agregar un listener de evento de selecciion a la lista de listeners.
	 * @param l Objeto de tipo ElementSelectedListener a agregar.
	 * 
	 */	
	public void addElementSelectedListener(ElementSelectedListener l);

	/**
	 * 
	 * Este m�todo se encarga de cargar en la vista web una url.
	 * @param url String a cargar en la vista web.
	 * 
	 */	
	public void loadWebView(String url);
}
