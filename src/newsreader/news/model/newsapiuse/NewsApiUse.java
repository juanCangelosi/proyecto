package newsreader.news.model.newsapiuse;

import java.util.List;

import newsreader.news.model.feeds.Feed;

public interface NewsApiUse {
	public List<Feed> useAPI();
	
	public void addUrl(String url);
}
