package newsreader.news.model;

import newsreader.news.model.newsapiuse.Creator;


/**
 * Class MainModel
 * 
 * Esta clase es el punto de entrada a la capa Modelo. Se encarga de instanciar el NewsModel concreto.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */
public class MainModel {
	private static MainModel instance;

	private MainModel(){

	}

	public static MainModel getInstance(){
		if(instance==null){
			instance=new MainModel();
		}
		return instance;
	}

	public NewsModel getNewsModel(){
		NewsModelImp modelo =  new NewsModelImp();
		Creator inicializarAPIs = new Creator();
		modelo.setAPIList(inicializarAPIs.getApis());
		return modelo;
	}
}
