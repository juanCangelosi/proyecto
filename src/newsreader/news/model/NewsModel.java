package newsreader.news.model;

import java.util.List;

import newsreader.news.model.feeds.Feed;

/**
 * Interface NewsModel 
 * 
 * Esta es la interfaz p�blica del modelo de noticias. Expone el m�todo de obtenci�n de noticias.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */
public interface NewsModel {

	/**
	 * 
	 * Este m�todo se encarga de parsear el XML y de construir un arreglo de objetos Feed. 
	 * 
	 * @return
	 * 		  arreglo de tipo Feed conteniendo todas las noticias encontradas a partir de la propiedad privada ATOM_URL.
	 */
	public List<Feed> getNews();
}
