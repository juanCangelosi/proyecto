package newsreader.news.model.feeds;

import com.comision4.newfetcher.model.AtomFeed;

public class AtomFeedAdapter extends Feed {
	
	public AtomFeedAdapter(AtomFeed f){
		super(f.getTitle(),f.getLink());
	}

}

