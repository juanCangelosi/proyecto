package newsreader.news.presenter;

import java.util.List;

import newsreader.news.model.NewsModel;
import newsreader.news.model.feeds.Feed;
import newsreader.news.view.ElementSelectedListener;
import newsreader.news.view.MainScreen;

public class MainScreenPresenter implements ElementSelectedListener {

	private MainScreen mainScreen;
	private NewsModel newsModel;
	private List<Feed> news;

	/**
	 * 
	 * Se encarga de inicializar una propiedad privada de tipo MainsScreen, y una de tipo NewsModel y delega el resto de la inicializaci�n
	 *  al metodo init().
	 * 
	 * @param ms
	 *            objeto de tipo MainScreen que ser� la componente principal de la vista.
	 * @param nm
	 *            objeto de tipo NewsModel que ser� la componente principal del modelo.
	 * 
	 */
	protected MainScreenPresenter(MainScreen ms, NewsModel nm){
		mainScreen=ms;
		newsModel=nm;
		init();
	}

	/**
	 * 
	 * Este m�todo se encarga de pasar su propia referencia a MainScreen como listener de eventos de seleccion de elementos.
	 * Ademas, se llama a updateNews() para que al momento de inicializar el sistema tambi�n se actualicen las noticias.
	 * 
	 */	
	protected void init(){
		mainScreen.addElementSelectedListener(this);
		updateNews();
	}


	public void onElementSelected(int index) {
		mainScreen.loadWebView(news.get(index).getLink());

	}

	/**
	 * 
	 * Se obtienen las noticias del modelo. Se guardan las noticias en una propiedad privada news. 
	 * Luego, se actualiza la vista con dichas news adaptadas.
	 * 
	 */
	private void updateNews(){
		news=newsModel.getNews();
		mainScreen.setNews(adaptFeeds());


	}

	/**
	 * 
	 * Este m�todo transforma una lista de objetos Feed en una lista de
	 * String, donde cada string resultante es el title del objeto Feed.
	 * 
	 * @return
	 * 		  arreglo de tipo String conteniendo todos los titulos de las noticias almacenadas en la propiedad privada news.
	 */
	private String[] adaptFeeds(){
		String[] noticias=new String[news.size()];
		int i=0;
		for(Feed f : news){
			noticias[i]=f.getTitle();
			i++;
		}
		return noticias;
	}





}
