package newsreader.news.view;

/**
 * Class MainView
 * 
 * Esta clase es el punto de entrada a la capa Vista. Se encarga de instanciar los Screen concretos.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */

public class MainView {
	private static MainView instance;

	private MainView(){

	}

	public static MainView getInstance(){
		if(instance==null){
			instance=new MainView();
		}
		return instance;
	}

	public MainScreen getMainScreen(){
		return new MainScreenImpl();
	}
}
