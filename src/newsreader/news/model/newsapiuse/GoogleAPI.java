package newsreader.news.model.newsapiuse;

import java.util.LinkedList;
import java.util.List;

import apilogic.GoogleNewsAPI;
import apilogic.GoogleNewsModel;
import apimodel.GoogleAPIFeed;
import newsreader.news.model.feeds.Feed;
import newsreader.news.model.feeds.GoogleFeedAdapter;

class GoogleAPI implements NewsApiUse{
	private List<String> urls;
	
	public GoogleAPI(){
		urls= new LinkedList<String>();
	}
	
	public List<Feed> useAPI(){
		List<Feed> feeds = new LinkedList<Feed>();
		GoogleNewsModel modelo = GoogleNewsAPI.getInstance().getModel(); //obtengo el modelo de la api
		for(String url: urls){
			modelo.setURL(url);
		}
		
		GoogleAPIFeed[] feedsGoogle=modelo.getNews(); //obtengo noticias de la url
		for(int i=0; i<feedsGoogle.length; i++){ //adapto el tipo de feed recibido al que utilizo
			 feeds.add(new GoogleFeedAdapter(feedsGoogle[i]));
		 }
		return feeds;
	}
	
	public void addUrl(String url){
		urls.add(url);
	}
}
