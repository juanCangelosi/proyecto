package newsreader.news.model.feeds;


import rssfeed.api.model.Feed_RSS;

public class RSSFeedAdapter extends Feed {
	
	public RSSFeedAdapter(Feed_RSS f){
		super(f.getTitle(),f.getLink());
	}
}
